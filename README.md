# stratmed: automated e2e tests

This project contains the end-to-end tests for **GLAS1** and **GLAS2** applications. Also included is a shell script ```auto_test_runner.sh``` to automate e2e tests and a custom Protractor plugin to notify applicable personnel on a spec (test) failure. This script will be later implemented in ```crontab``` or [Supervisor: A Process Control System](http://supervisord.org/) for the sake of efficiency

---

### Testing Requirements

1. [NodeJS](https://nodejs.org/)

These requirements assume you are familiar with node.js. To install protractor and run the e2e tests locally, follow the commands below after navigating to the root of the project via a command prompt

Install Protractor
```
npm install -g protractor protractor-html-screenshot-reporter --save-dev
```
Make sure protractor is installed correctly  
```
protractor --version
```
Update the binaries for selenium   
```
sudo webdriver-manager update
```
Start up a selenium server instance    
```
sudo webdriver-manager start
```
You will also need to create an "env.js" file, in accordance with the "env.sample.js" provide.

To run the tests, open up another terminal window in the test project root and run  
```
protractor conf.js
```

### Reports

I have also implemented a Protractor plugin which auto-generates reports in HTML with screenshots. Reports can be viewed by opening ```reports/report.html``` in your browser after a test is run
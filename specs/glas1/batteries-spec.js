// Assume this is run after tests-spec.js

var initializeBatteryTests = function(battery)
{
    var test = battery.testSequence;
    do {
        describe('Take Test "' + test.name + '"', function() {
            it('Answer all questions', function () {
                //don't go anywhere with the url because the tests are administered in sequence
                takeTest(test);
            });
        });
        test = test.nextTest;
    } while(test !== null);
};

var initializeBattery = function(battery)
{
    describe('Take Battery "' + battery.name + '"', function() {
        it('Complete All Tests', function () {
            //click the battery button
            browser.get(suite.batterySource);
            var index = 0;
            for (var button in collectBatteries(false)) {
                if (index++ === battery.buttonIndex) {
                    button.click().then(function() {
                        initializeBatteryTests(battery);
                    });
                }
            }
        });
    });
};

/**
 * All batteries that are run or are to run are registered here.
 * @type {Array}
 */
batteries = [];

/**
 *
 * @param batteryButton
 * @param buttonIndex
 * @todo check that all expected tests are present.
 * @todo gendered batteries.
 */
var buildBattery = function(batteryButton, buttonIndex)
{
    batteryButton.element(by.className(css.tests.testName)).getText().then(function(batteryName) {

        var battery = {
            name: batteryName,
            finished: false,
            gender: 'neutral',
            buttonIndex: buttonIndex,
            testSequence: null
        };
        batteries.push(battery);
        //create the tests for this battery
        batteryButton.element(by.className(css.tests.testList)).getText().then(function(testsStr) {
            var testNames = testsStr.split(', ');
            for (var name in testNames) {
                var test = buildTest(name);
                setNextTest(battery.testSequence, test);
                initializeBattery(battery);
            }
        });
    });
};

/**
 * @todo Need to collect the special batteries at the bottom of the test screen.
 */
var collectBatteries = function(build)
{
    browser.get(suite.batterySource);
    //the batteries aren't organized nicely.
    var primaryContainer = element(by.xpath(xpath.tests.batteryContainer));
    var primaryIndex; //keep track of the ongoing index above the callback function scope
    var buttons = [];
    primaryContainer.all(by.className(css.tests.buttonClassName)).each(function (button, index) {
        if (build) {
            buildBattery(button, index);
        } else {
            buttons.push(button);
        }
    }).then(function() {
        var secondaryContainer = element(by.xpath(xpath.tests.batteryContainer2));
        secondaryContainer.all(by.className(css.tests.secondaryButtonClassName)).each(function (button, index) {
            if (build) {
                buildBattery(button, primaryIndex + index);
            } else {
                buttons.push(button);
            }
        });

        return buttons;
    });
};

describe('Battery Tests', function () {

    beforeEach(function () {
        beforeEachDelegate(suite.batterySource);
    });

    it('Collect All Batteries to Test', function() {
        collectBatteries(true);
    });

});
// Assume this is run after authentication-spec.js so that the session is preserved

suite = {
    testSource: 'http://' + env.host + env.uri.tests,
    batterySource: 'http://' + env.host + env.uri.batteries,
    ec: protractor.ExpectedConditions
};

takeTest = function(test)
{
    //check for the patient info screen
    var firstname = element(by.id(css.patient.firstname));
    firstname.isPresent().then(function(present) {
        if (present === false) { //a wild gender selection page appears
            //create a duplicate but male test
            var maleTest = buildTest(test.name, test.buttonIndex);
            maleTest.gender = 'male';
            //queue the new unit test
            var queueTest = function(url, test) {
                describe('Male variant of "' + test.name + '"', function () {
                    it('Answer all questions', function () {
                        browser.get(url);
                        selectGender(test);
                    });
                });
            };
            browser.getCurrentUrl().then(function(url) {
                queueTest(url, maleTest); //send straight to this gender selection page
            });
            //set the original test to female
            test.gender = 'female';
        }
        //handle the gender selection buttons (or lack thereof)
        selectGender(test);
    });
};

selectGender = function(test)
{
    var genderXPath;
    switch (test.gender) {
        case 'female':
            genderXPath = xpath.tests.female;
            break;
        case 'male':
            genderXPath = xpath.tests.male;
            break;
        case 'neutral':
        default:
            //you're on the patient info page, don't try to click a gender button
            fillPatientInfo(test);
            return;
    }
    element(by.xpath(genderXPath)).click().then(function() {
        fillPatientInfo(test);
    });
};

/**
 * Fill out the patient info screen, then start answering questions.
 * @param test
 */
fillPatientInfo = function(test)
{
    element(by.id(css.patient.firstname)).sendKeys(env.user.firstname);
    element(by.id(css.patient.lastname)).sendKeys(env.user.lastname);
    element(by.id(css.patient.MRN)).sendKeys(env.user.MRN);
    element(by.xpath(xpath.tests.month)).click();
    element(by.xpath(xpath.tests.day)).click();
    element(by.xpath(xpath.tests.year)).click();
    element(by.id(css.patient.submit)).click().then(function () {
        answerQuestion(test, 1);
    });
};

/**
 * Factory Function to build a question.
 * @param test
 * @param questionNumber
 */
answerQuestion = function(test, questionNumber)
{
    var question = {
        id:  css.tests.questionContainer + questionNumber,
        number: questionNumber,
        numQuestions: 0,
        type: '', //radio/checkbox
        answers: [],
        title: '',
        answerOptions: {} // id: copy
    };
    //add the question to the test's question queue
    test.questions.push(question);
    //determine the number of questions and continue
    var questionContainer = element(by.css('#' + question.id));
    //get the question title
    questionContainer.element(by.css(css.tests.title.replace('<id>', question.number))).getText().then(function(title) {
        question.title = sanitizeLabel(title);
        //@todo verify this is the correct question
        //determine the question type (radio/checkbox)
        var questionList = questionContainer.all(by.className(css.tests.questionWrapper));
        questionList.count().then(function(count) {
            if (count === 0) {
                question.type = 'checkbox';
                pickCheckbox(test, question, questionContainer);
            } else {
                question.type = 'radio';
                pickRadio(test, question, count);
            }
        });
    });
};

/**
 * Select a radio answer, click it, then continue.
 * @param test
 * @param question
 * @param count
 */
pickRadio = function(test, question, count)
{
    question.numQuestions = count;
    //pick a random integer between 1 and question.numQuestions (inclusive)
    var answer = Math.floor(Math.random() * question.numQuestions) + 1;
    question.answers.push(answer);
    //store the question's answer options
    var busy = 0; //semaphor, necessary for tracking async callback completion
    var selectedAnswerLabel;
    var baseAnswerXPath = xpath.tests.radioAnswerLabel.replace('<id>', question.id);
    for (var i = 1; i <= question.numQuestions; i++) { //questions start from 1
        busy++;
        var answerLabel = element(by.xpath(baseAnswerXPath.replace('<answer>', i.toString())));
        //if this was the selected answer, store it for later
        if (i === answer) {
            selectedAnswerLabel = answerLabel;
        }
        answerLabel.getText().then(function(label) {
            //"i" will be numQuestions + 1 at this point, so subtracting "busy" (starts at numQuestions),
            //will give the correct index. Alternative way to do this would be with yet another lambda
            //function, but there are already a ton of those. Just be careful considering how tightly
            //integrated "i" and the "busy" semaphor are with the logic here.
            question.answerOptions[i - busy] = sanitizeLabel(label);
            //after all the answers are stored, click the lucky winner
            if (--busy === 0) {
                // @todo check that these were the expected answers
                selectedAnswerLabel.click().then(function() {
                    findNextButton(test, question);
                });
            }
        });
    }
};

/**
 * Cleans up text read from the page.
 * @param str
 * @returns {string}
 */
sanitizeLabel = function(str)
{
    return str.trim(); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trim#Browser_compatibility
};

/**
 * Recursively select between 0 and all of the available checkbox answers before continuing.
 * Note that checkbox answers are cumulative, they don't start over at 1 each time like the radios.
 * @param test
 * @param question
 * @param questionContainer
 */
pickCheckbox = function(test, question, questionContainer)
{
    var questionList = questionContainer.all(by.className(css.tests.cbWrapper));
    questionList.count().then(function(count) {
        //find the range of answer ids for this question.
        question.numQuestions = count;
        var min = getTotalAnswers(test);
        var max = min + count;
        //collect answer choices and store their copy in the question object
        var busy = 0;
        var checkboxes = {};
        for (var i = min + 1; i <= max; i++) { //min is not a valid choice!
            busy++;
            //populate a map of checkboxes so the pickAnswer function doesn't have to look them up again
            var checkboxXPath = xpath.tests.checkbox.replace('<id>', i.toString());
            var checkbox = questionContainer.element(by.xpath(checkboxXPath));
            checkboxes[i] = checkbox;
            //store the answer text
            var labelXPath = 'ancestor::div[@class="' + css.tests.cbWrapper + '"]'; //technically not the label, but close enough
            checkbox.element(by.xpath(labelXPath)).getText().then(function(label) {
                question.answerOptions[i - busy] = sanitizeLabel(label); //see pickRadio()
                //recursively select answers once all the questions have been stored
                if (--busy === 0) {
                    pickAnswer();
                }
            });

        }
        var pickAnswer = function()
        {
            //remember this is INCLUSIVE! Picking the invalid "min" means don't choose anything.
            var answer = Math.floor(Math.random() * (max - min + 1)) + min;
            //"min" means don't choose any(more) answers. Also don't pick one already picked.
            if (answer !== min && question.answers.indexOf(answer) < 0) {
                question.answers.push(answer);
                checkboxes[answer].click().then(function () {
                    pickAnswer();
                });
            } else {
                findNextButton(test, question);
            }
        };
    });
};

/**
 * Counts up all the answers TO ANSWERED QUESTIONS so far. Needed for proper checkbox selection,
 * since checkbox answers are cumulative between questions.
 * @param test
 * @returns {number}
 */
getTotalAnswers = function(test)
{
    var answers = 0;
    for (var i = 0; i < test.questions.length - 1; i++) { //don't count the current question
        //only checkbox answers are cumulative
        if (test.questions[i].type === 'checkbox') {
            answers += test.questions[i].numQuestions;
        }
    }
    return answers;
};

/**
 * Determine which variation of the "Next" button is present. Works like a Chain-Of-Responsibility.
 * @todo Give all next buttons a "next-button" class >:[
 * @todo Test back button
 * @param test
 * @param question
 */
findNextButton = function(test, question)
{
    var button = element(by.css('#' + question.id + ' .' + css.tests.nextButton));
    button.isPresent().then(function(present) {
        if (present === false) {
            button = element(by.css('#' + question.id + ' .' + css.tests.nextButton2));
            button.isPresent().then(function(present) {
                if (present === false) {
                    button = element(by.css('#' + question.id + ' ' + css.tests.nextButton3));
                    button.isPresent().then(function(present) {
                        if (present === false) {
                            var nextId = question.number + 1 + '';
                            button = element(by.xpath(xpath.tests.nextButton4.replace('<nextId>', nextId)));
                            button.isPresent().then(function(present) {
                                if (present === false) {
                                    console.log('BUTTON NOT FOUND FOR TEST "' + test.name + '" question ' + question.number);
                                } else {
                                    clickNext(test, question, button);
                                }
                            });
                        } else {
                            clickNext(test, question, button);
                        }
                    });
                } else {
                    clickNext(test, question, button);
                }
            });
        } else {
            clickNext(test, question, button);
        }
    });
};

/**
 * Once a "next" button is identified, this function clicks it
 * and then recurses back to answerQuestion() or breaks out if the test is at an end.
 * @param test
 * @param question
 * @param nextButton
 */
clickNext = function(test, question, nextButton)
{
    //@todo check for hidden buttons that signify branches
    nextButton.getText().then(function(buttonText) {
        nextButton.click().then(function() {
            if (buttonText === messages.next) {
                answerQuestion(test, question.number + 1);
            } else {
                finishTest(test);
            }
        });
    });
};

/**
 * Batteries have multiple sections (tests) in which the question numbers start over.
 * @param test
 */
finishTest = function(test)
{
    test.finished = true;
    //if the "enter pin" screen is not present, there is another test in the battery
    element(by.xpath(xpath.tests.completedPin)).isPresent().then(function(present) {
        if (present === false) {
            answerQuestion(test.nextTest, 1);
        } else {
            checkResults(test);
        }
    });
};

/**
 * Check the results of the test.
 * @param test
 * @todo Actually check them
 */
checkResults = function(test)
{
    element(by.xpath(xpath.tests.completedPin)).sendKeys('invalidpin').then(function(){
        element(by.xpath(xpath.tests.submitTest)).click().then(function(){
            //expect(element(by.xpath(xpath.tests.pinMessage))).toEqual(messages.invalidPin);
            //@todo determine why protractor is hanging on the previous line.

            expect(test.name).toEqual(test.name);

            element(by.xpath(xpath.tests.completedPin)).sendKeys(env.user.pin);
        });
    });
};

/**
 * All tests that are run or are to run are registered here.
 * @type {Array}
 */
tests = [];

/**
 * Factory function to build a test. Stores the test in the global registry.
 * @param testName
 * @param buttonIndex
 * @returns {{name: *, finished: boolean, gender: string, buttonIndex: *, questions: Array, nextTest: *}}
 */
buildTest = function(testName, buttonIndex)
{
    var test = {
        name: testName,
        finished: false,
        gender: 'neutral',
        buttonIndex: buttonIndex,
        questions: [],
        nextTest: null
    };
    tests.push(test);

    return test;
};

setNextTest = function(testSequence, test)
{
    if (testSequence.nextTest === null) {
        testSequence.nextTest = test;
    } else {
        setNextTest(testSequence.nextTest, test);
    }
};

/**
 * Delegate function for the beforeEach() Jasmine event, turns off angular.
 */
beforeEachDelegate = function()
{
    browser.ignoreSynchronization = true;
};

var initializeTest = function(test)
{
    describe('Take Test "' + test.name + '"', function() {
        it('Answer all questions', function() {
            browser.get(suite.testSource);
            //refresh the button list
            var testContainer = element(by.xpath(xpath.tests.testContainer));
            testContainer.all(by.className(css.tests.buttonClassName)).each(function (startButton, buttonIndex) {
                //ORT (checkboxes) is #8 (starts from 0)
                if (buttonIndex === test.buttonIndex && buttonIndex > -1) { //if you want to skip to a specific test
                    startButton.click().then(function() {
                        takeTest(test);
                    });
                }
            });
        });
    });
};

var collectTests = function()
{
    browser.get(suite.testSource);
    var testContainer = element(by.xpath(xpath.tests.testContainer));

    testContainer.all(by.className(css.tests.buttonClassName)).each(function (button, buttonIndex) {
        //@todo check that all expected tests are present.

        button.element(by.className(css.tests.testName)).getText().then(function(testName) {
            var test = buildTest(testName, buttonIndex);
            initializeTest(test);
        });
    });
};

describe('Standalone Tests', function () {

    beforeEach(function () {
        beforeEachDelegate();
    });

    it('Collect All Tests to Test', function() {
        collectTests();
    });

});

/* global browser, element, by, expect, protractor */

describe('phq4', function () {

    /**
     * Global variables & aliases for the test suite (protractor)
     * @type Object
     */
    var suite = {
        source_login: 'http://greenlightmed.com/m/#login',
        source: 'http://greenlightmed.com/m/secure/tests/phq4.php',
        /**         
         * Represents a library of canned expected conditions that are 
         * useful for protractor, especially when dealing with non-angular apps.
         */
        ec: protractor.ExpectedConditions
    };
    
    /* @todo query db */

    /**
     * Shortcuts and helpers for accessing the DOM of the suite.source
     * @type Object
     */
    var dom = {
        firstname: element(by.id('firstname')),
        lastname: element(by.id('lastname')),
        medicalrecordnumber: element(by.id('medicalrecordnumber')),
        username: element(by.id('username')),
        password: element(by.id('password')),
        submit: element(by.css('[type="submit"]')),
        pin: element(by.id('testPin')),
        begintest: element(by.id('begintest'))
    };

    beforeEach(function () {
        // turn off protractor trying to locate AngularJS on the page
        browser.ignoreSynchronization = true;
    });

    it('should login user', function () {
        // navigate to source page
        browser.get(suite.source_login);

        // enter credentials
        dom.username.sendKeys('shane');
        dom.password.sendKeys('Cust456');

        // click the submit button
        dom.submit.click();
    });

    it('should fill in basic patient information', function () {
        // navigate to source page
        browser.get(suite.source);

        // wait for browser DOM to be accessible. This is to work around
        // routing techniques used by AngularJS, JQuery, etc.
        browser.wait(suite.ec.presenceOf(dom.firstname), 10000);

        // fill in the information
        dom.firstname.sendKeys('Test');
        dom.lastname.sendKeys('Test');
        dom.medicalrecordnumber.sendKeys('1234565');
        element(by.xpath('//*[@id="month"]/option[3]')).click();
        element(by.xpath('//*[@id="date"]/option[10]')).click();
        element(by.xpath('//*[@id="year"]/option[30]')).click();

        // start the test
        dom.begintest.click();
    });

    it('should answer the 1st question', function () {
        browser.wait(suite.ec.presenceOf(element(by.id('question1'))), 10000);
        element(by.css('#question1 .choice2label')).click();
        element(by.css('#question1 .group-1-next a')).click();
    });

    it('should answer the 2nd question', function () {
        browser.wait(suite.ec.presenceOf(element(by.id('question2'))), 10000);
        element(by.css('#question2 .choice2label')).click();
        element(by.css('#question2 .group-2-next a')).click();
    });

    it('should answer the 3rd question', function () {
        browser.wait(suite.ec.presenceOf(element(by.id('question3'))), 10000);
        element(by.css('#question3 .choice2label')).click();
        element(by.css('#question3 .group-3-next a')).click();
    });

    it('should answer the 4th question', function () {
        element(by.css('#question4 .choice2label')).click();
        element(by.css('#question4 .group-4-next a')).click();
    });

    it('should enter pin and view tests', function () {
        dom.pin.sendKeys('1111');
        dom.element(by.id('testPinsubmit')).click();
    });
});
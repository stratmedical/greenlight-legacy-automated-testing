/* global browser, element, by, expect, protractor */


describe('Authentication', function () {

    /**
     * Variables & aliases for the test suite (protractor)
     * @type Object
     */
    var suite = {
        source: 'http://' + env.host + env.uri.login,
        logout: 'http://' + env.host + env.uri.logout,
        /**         
         * Represents a library of canned expected conditions that are 
         * useful for protractor, especially when dealing with non-angular apps.
         */
        ec: protractor.ExpectedConditions
    };

    /**
     * Shortcuts and helpers for accessing the DOM of the suite.source
     * @type Object
     */
    var dom = {
        username: element(by.id('username')),
        password: element(by.id('password')),
        submit: element(by.xpath(xpath.auth.login))
    };

    beforeEach(function () {
        // turn off protractor trying to locate AngularJS on the page
        browser.ignoreSynchronization = true;

        // navigate to source page
        browser.get(suite.logout);
        browser.get(suite.source);

        // wait for browser DOM to be accessible. This is to work around
        // routing techniques used by AngularJS, JQuery, etc.
        browser.wait(suite.ec.presenceOf(dom.username), 10000);

        // make sure the username input element is present
        expect(dom.username.isPresent()).toBeTruthy();
    });

    it('Check username and password field types', function () {
        // make sure the attribute types are correct
        expect(dom.username.getAttribute('type')).toEqual('text');
        expect(dom.password.getAttribute('type')).toEqual('password');
    });

    it('Do not allow invalid logins', function() {
        dom.username.sendKeys('asdfsdf');
        dom.password.sendKeys('kjsdjkhfd');
        dom.submit.click().then(function() {
            expect(element(by.xpath(xpath.auth.invalidLogin)).getText()).toEqual(messages.invalidLogin);
        });
    });

    it('User can log in', function () {
        dom.username.sendKeys(env.user.login);
        dom.password.sendKeys(env.user.password);
        dom.submit.click().then(function(){
            //will there always be a notice?
            var noticeButton = element(by.xpath(xpath.auth.noticeButton));
            noticeButton.click().then(function(){
                //check that all 6 navigation buttons are present
                for (var i in xpath.auth.navigation) {
                    expect(element(by.xpath(xpath.auth.navigation[i]))).toBeTruthy();
                }
            });
        });
    });

    //it('Do allow multiple unsuccessful logins', function () {
        // @todo make sure server-side capability is in place first
    //});
});
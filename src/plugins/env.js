/* environment values that should be excluded from the repo */

env = {
    host: "104.236.117.39", //staging server
    uri: {
        login: "/m/#login", //glas1 urls
        logout: "/m/logout.php",
        tests: "/m/secure/individual_tests.php",
        batteries: "/m/secure/batteries.php"
    },
    user: {
        login: "shane", //staging server credentials
        password: "Cust456",
        firstname: 'First Name',
        lastname: 'Last Name',
        MRN: '1234565',
        pin: '1111'
    }
};
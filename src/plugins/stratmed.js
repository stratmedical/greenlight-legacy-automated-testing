/* global exports */

exports.setup = function (config) {
    // @todo implement stratmed.setup() logic
};

exports.teardown = function (config) {
    // @todo implement stratmed.teardown() logic
};

exports.postResults = function (config) {
    // @todo implement stratmed.postResults() logic
};

exports.postTest = function (config, passed, testInfo) {
    // @todo implement stratmed.postTest() logic
};

exports.name = 'stratmed';

css = {
    patient: {
        firstname: 'firstname',
        lastname: 'lastname',
        MRN: 'medicalrecordnumber',
        submit: 'begintest'
    },
    tests: {
        testName: 'button_label',
        testList: 'button_description',
        buttonClassName: "primary_button",
        secondaryButtonClassName: "secondary_button", //@todo refactor batteries section layout
        questionWrapper: "ui-radio",
        cbWrapper: 'ui-checkbox',
        nextButton: 'ui-nextbutton',
        nextButton2: 'nextbutton',
        nextButton3: '#submittingbutton button',
        questionContainer: 'question',
        choice: 'choice',
        title: '#testquestion<id>'
    }
};

messages = {
    invalidLogin: "Invalid user name or password",
    invalidPin: "Invalid PIN",
    next: "Next"
};

//FYI Firebug (Firefox addon) makes it super easy (right click context menu) to get the XPath for elements.
xpath = {
    auth: {
        login: "/html/body/div[2]/div[2]/form/div[4]/button",
        invalidLogin: "/html/body/div[2]/div[2]/form/h4",
        noticeButton: "/html/body/div[2]/div/ul[2]/ul/center/button[2]",
        navigation: {
            tests: "/html/body/div[1]/div[1]/div/div[1]/button[2]",
            results: "/html/body/div[1]/div[1]/div/div[1]/button[3]",
            patients: "/html/body/div[1]/div[1]/div/div[2]/button[1]",
            help: "/html/body/div[1]/div[1]/div/div[2]/button[2]",
            logout: "/html/body/div[1]/div[1]/div/div[2]/button[3]"
        }

    },
    tests: {
        testContainer: '//*[@id="header"]',
        batteryContainer: '/html/body/div[1]/div[3]/div/div[2]',
        batteryContainer2: '/html/body/div[1]/div[3]/div/div[4]',
        month: '//*[@id="month"]/option[3]',
        day: '//*[@id="date"]/option[10]',
        year: '//*[@id="year"]/option[30]',
        completedPin: '//*[@id="testPin"]',
        submitTest: '//*[@id="testPinsubmit"]',
        pinMessage: '//*[@id="testPinmsg"]',
        female: '/html/body/div[1]/div[2]/a[2]',
        male: '/html/body/div[1]/div[2]/a[1]',
        checkbox: '//input[@name="group-<id>"]',
        radioAnswerLabel: '//*[@id="<id>"]//label[@for="choice<answer>"]',
        nextButton4: '//a[@href="#question<nextId>"]'
    }
};
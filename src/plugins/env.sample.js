/* environment values that should be excluded from the repo. You need to create an "env.js" copy. */

env = {
    host: "",
    uri: {
        login: "", //i.e. "/m/#login" for glas1 (no hostname, just the uri)
        logout: "",
        tests: "",
        batteries: ""
    },
    user: {
        login: "", //authentication credentials
        password: "",
        firstname: '', //patient info
        lastname: '',
        MRN: '',
        pin: '' //doctor pin at the end of the test
    }
};
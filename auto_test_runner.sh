#!/bin/sh

# @todo this functionality should really be handled by cron
# since this script may deem unreliable if run manually

while [ true ]
do
    # @todo implement daemon logic correctly
    protractor conf.js
    # sleep for 55 seconds (just for testing)
    sleep 55
done
/* global exports, jasmine */

/**
 * An npm module and grunt plugin which generates your 
 * Protractor test reports in HTML with screenshots
 * @type HtmlReporter
 */
var HtmlReporter = require('protractor-html-screenshot-reporter');

/**
 * Configuration object required for Protractor testing
 * @type Object
 */
exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: [
        'specs/glas1/authentication-spec.js',
        'specs/glas1/tests-spec.js',
        //'specs/glas1/batteries-spec.js' //not quite ready
    ],
    capabilities: {
        'browserName': 'firefox'
    },
    plugins: [
        /**
         * @todo implement stratmed Protractor plugin logic
         * Custom plugin to notify IT administrative personnel in the
         * event of an "automated test" failure.
         */
        {
            path: 'src/plugins/env.js'
        },
        {
            path: 'src/plugins/stratmed.js'
        }

    ],
    onPrepare: function () {
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'reports/'
        }));
    },
    /**
     * Options to be passed to Jasmine
     * @returns null
     */
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
};